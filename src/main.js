import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./scss/index.scss";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";

// Import Bootstrap an BootstrapVue CSS files (order is important)
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "../node_modules/bootstrap-vue/dist/bootstrap-vue";

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");
